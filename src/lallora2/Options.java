package lallora2;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Options")
public class Options extends HttpServlet {
	private static String userName = "";
	private static int userAmount = 0;
	private static int userAddAmount = 0;
	private static int userGetAmount = 0;
	private static final long serialVersionUID = 1L;
       
    @Override
    public void init() throws ServletException {
    	super.init();
    }

	/**
	 * принимает и выдает в ответ /Options
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		userName = Authorization.ua.getLogin();
		userAmount = Authorization.ua.getAmount();
		request.setCharacterEncoding("UTF-8");
		try
		{
			userAddAmount = Integer.parseInt(request.getParameter("addAmount"));
			}
		catch ( NumberFormatException e )
		{
			userAddAmount = 0;
		}
		try
		{
			userGetAmount = Integer.parseInt(request.getParameter("getAmount"));
			}
		catch ( NumberFormatException e )
		{
			userGetAmount = 0;
		}
		
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");		
		userAmount = userAmount + userAddAmount - userGetAmount;
		Authorization.ua.setAmount(userAmount);
		if(userAddAmount>0) {
			String description = "Добавлено на счёт: " + userAddAmount;
			MakeHistoryRecord.makeHistoryRecord(userName, description, new Date());
		}
		if(userGetAmount>0) {
			String description = "Снято со счёта: " + userGetAmount;
			MakeHistoryRecord.makeHistoryRecord(userName, description, new Date());
		}
		if(MyDB.chConnect) {
	        PrintWriter out = response.getWriter();
			out.print(MyHTML.firstResponseStr);
			out.print("Имя пользователя: <h3>" + userName + "</h3>");
			out.print("Сумма на счете: <h3>" + userAmount + " руб.</h3><br>");
			//out.println("<p><a href='menu.html' target='CONTENT'>Мой профиль</a></p>");
			out.println("<b>Выполнение операций</b><br/><br/>"
					+"<form method='POST' action='Options'><table><tr><td>Добавить сумму:</td><td><input type='text' name='addAmount'></td>"
					+"</tr><tr><td>Снять сумму:</td><td><input type='text' name='getAmount'></td></tr><tr><td></td>"
					+"<td><input type='submit' value='Выполнить'></td></tr></table></form>");
			out.println("<br/><br/>");
			out.println("<b>Перевод другому пользователю</b><br/><br/>"
					+"<form method='POST' action='Transfer'><table><tr>"
					+"<td>Логин другого пользователя:</td><td><input type='text' name='recipient'></td></tr>"
					+"<tr><td>Перевести сумму:</td><td><input type='text' name='sendAmount'></td></tr><tr><td></td>"
					+"<td><input type='submit' value='Выполнить перевод'></td></tr></table></form><br/><br/>");
			out.print(MyHTML.buttonExit);
			out.close();
		} else {
			System.out.println("Не удалось установить соединение с базой данных");
		}
	}
}
