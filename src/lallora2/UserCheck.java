package lallora2;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;

final class UserCheck {
	private String login;
	private String passw;
	private int amount = 0;
	private boolean chPassw = false;
	
	public UserCheck(String login, String passw) {
		this.login=login;
		this.passw=passw;
	}
	
	public UserCheck(String login) {
		this.login=login;
	}

	public boolean checkUser() {
		ResultSet rs = null;
		Statement st = null;
		boolean ch = false;
		try{
			st = MyDB.conDB.createStatement();
	        rs = st.executeQuery("select * from clients");
	        while (rs.next()) {
	        	if(rs.getString(2).contentEquals(login)&&rs.getString(3).contentEquals(passw)) {
	        		amount = Integer.parseInt(rs.getString(4));
	        		chPassw = true;
	        		ch=true;
	        	}
	        }
		  } catch (Exception e){
			  e.printStackTrace(); 
			  System.out.println("Ошибка выполнения checkUser");
		  }
		  finally {
			  if(!ch) {
				  System.out.println("Авторизация " + login + " невозможна");
			  } else {
				  System.out.println("Пользователь " + login + " был найден и авторизован.");
			  }
		  }
		return ch;
	}		
	
	public boolean existUser() {
		ResultSet rs = null;
		Statement st = null;
		boolean ch = false;
		try{
			st = MyDB.conDB.createStatement();
	        rs = st.executeQuery("select * from clients");
	        while (rs.next()) {
	        	if(rs.getString(2).contentEquals(login)) {
	        		ch=true;
	        	}
	        }
		  } catch (Exception e){
			  e.printStackTrace(); 
			  System.out.println("Ошибка выполнения existUser");
		  }
		  finally {
			  if(!ch) {
				  System.out.println("Нет пользователя " + login);
			  } else {
				  System.out.println("Пользователь " + login + " был найден");
			  }
		  }
		return ch;
	}		

	public boolean regUser(String usName, String usPassw) {
		Statement st = null;
		boolean ch = false;
		try{
			st = MyDB.conDB.createStatement();
	        String sql = "INSERT INTO CLIENTS (LOGIN, PASSWORD, AMOUNT) VALUES ('" + usName + "', '" + usPassw +"', 0)";
	        st.executeUpdate(sql);
			ch = true;
			String description = "Добавлен новый клиент: " + usName;
			MakeHistoryRecord.makeHistoryRecord(usName, description, new Date());
		} catch (Exception e){
			  e.printStackTrace(); 
			  System.out.println("Ошибка выполнения existUser");
		  }
		  finally {
			  if(!ch) {
				  System.out.println("Нет пользователя " + login);
			  } else {
				  System.out.println("Пользователь " + login + " был найден");
			  }
		  }
		return ch;
	}		

	public String getLogin() {return login;}
	
	public String getPassw() {
		if (chPassw) {
				return passw;
			}
		return "";
		}
	
	public int getAmount() {
		if (chPassw) {
			return amount;
		}
	return 0;
	}
	
	public void setAmount(int newAmount) {
		if (chPassw) {
			Statement st = null; 
		    try{
			    System.out.println("setAmount. Пользователь " + login + ". Старая сумма на счете: " + amount);
		    	String query = "UPDATE CLIENTS SET AMOUNT = " + newAmount + " WHERE LOGIN = '" + login + "' and PASSWORD = '" + passw + "'";
		        st = MyDB.conDB.createStatement();
		        st.executeUpdate(query);
		    	amount = newAmount;
			    System.out.println("setAmount. Пользователь " + login + ". Новая сумма на счете: " + amount);
				//String description = "Новая сумма на счёте: " + newAmount;
				//MakeHistoryRecord.makeHistoryRecord(login, description, new Date());
		    } catch (Exception e){
				  e.printStackTrace();
				  System.out.println("setAmount. Ошибка выполнения setAmount");
		  }
		}
	}

		public void addAmount(int addAmount) {
		ResultSet rs = null;
		Statement st = null;
		amount = 0;
		try{
			st = MyDB.conDB.createStatement();
	        rs = st.executeQuery("select * from clients");
	        while (rs.next()) {
	        	if(rs.getString(2).contentEquals(login)) {
	        		amount = Integer.parseInt(rs.getString(4)) + addAmount;
	        	}
	        }
		    System.out.println("addAmount. Пользователь " + login + ". Старая сумма на счете: " + amount);
			st = null; 
	    	String query = "UPDATE CLIENTS SET AMOUNT = " + amount + " WHERE LOGIN = '" + login + "'";
	        st = MyDB.conDB.createStatement();
	        st.executeUpdate(query);
		    System.out.println("addAmount. Пользователь " + login + ". Новая сумма на счете: " + amount);
			//String description = "Изменение суммы на счёте: " + addAmount;
			//MakeHistoryRecord.makeHistoryRecord(login, description, new Date());
		  } catch (Exception e){
			  e.printStackTrace(); 
			  System.out.println("addAmount. Ошибка выполнения addAmount");
		  }
	}
}
