package lallora2;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Transfer")
public class Transfer extends HttpServlet {
	private static String userName = "";
	public UserCheck ra;

	private static String recipientName = "";
	private static int sendAmount = 0;
	private static int userAmount = 0;
	private static final long serialVersionUID = 1L;
       
    @Override
    public void init() throws ServletException {
    	super.init();
    }

	/**
	 * принимает и выдает в ответ /Transfer
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		userName = Authorization.ua.getLogin();
		userAmount = Authorization.ua.getAmount();
		request.setCharacterEncoding("UTF-8");

		recipientName = request.getParameter("recipient");
		sendAmount = Integer.parseInt(request.getParameter("sendAmount"));
		System.out.println("Считано из HTML: recipientName = " + recipientName + " sendAmount = " + sendAmount);
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		if(userName.equals(recipientName)) {
			System.out.println("Такого типа перевода не существует");
			PrintWriter out = response.getWriter();
			out.print(MyHTML.firstResponseStr);
			out.print("<h3>Такого типа перевода не существует</h3>");
			out.print(MyHTML.lastResponseStr);
			out.close();
		}else {
			if(MyDB.chConnect) {
				System.out.println("myDB подключена");
				ra = new UserCheck(recipientName);
				if(ra.existUser()){
					System.out.println(recipientName + " существует и найден");
					ra.addAmount(sendAmount);
					System.out.println("ra.addAmount(sendAmount) отработала");
					userAmount = userAmount - sendAmount;
					Authorization.ua.setAmount(userAmount);
					String description = "Выполнен перевод Получателю " + recipientName + " сумма " + sendAmount;
					MakeHistoryRecord.makeHistoryRecord(userName, description, new Date());
					String description2 = "Получено от " + userName + " сумма " + sendAmount;
					MakeHistoryRecord.makeHistoryRecord(recipientName, description2, new Date());
					PrintWriter out = response.getWriter();
					out.print(MyHTML.firstResponseStr);
					out.print("Имя пользователя: <h3>" + userName + "</h3>");
					out.print("Сумма на счёте пользователя: <h3>" + userAmount + " руб.</h3><br>");
					//out.println("<p><a href='menu.html' target='CONTENT'>Мой профиль</a></p>");
					out.println("<b>Выполнение операции трансфера</b><br/><br/>"
							+"<form method='POST' action='Options'><table><tr><td>Добавить сумму:</td><td><input type='text' name='addAmount'></td>"
							+"</tr><tr><td>Снять сумму:</td><td><input type='text' name='getAmount'></td></tr><tr><td></td>"
							+"<td><input type='submit' value='Выполнить'></td></tr></table></form>");
					out.println("<br/><br/>");
					out.println("<b>Перевод другому пользователю</b><br/><br/>"
							+"<form method='POST' action='Transfer'><table><tr>"
							+"<td>Логин другого пользователя:</td><td><input type='text' name='recipient'></td></tr>"
							+"<tr><td>Перевести сумму:</td><td><input type='text' name='sendAmount'></td></tr><tr><td></td>"
							+"<td><input type='submit' value='Выполнить перевод'></td></tr></table></form><br/><br/>");
					out.print(MyHTML.buttonExit);
					out.close();
				}else {
					PrintWriter out = response.getWriter();
					out.print(MyHTML.firstResponseStr);
					out.print("<h3>Перевод невозможен. Нет такого клиента.</h3>");
					out.print(MyHTML.lastResponseStr);
					out.close();
					}
			} else {
				System.out.println("Не удалось установить соединение с базой данных");
			}
		}
	}
/*
	private void sqlGetUserData() {
		ResultSet rs = null;
		Statement st = null; 
		  try{
	            st = myDB.conDB.createStatement();
	            rs = st.executeQuery("select * from clients");
	            while (rs.next()) {
	            	if(rs.getString(2).contentEquals(userName)) {
	            		checkUserName = true;
	            		userAmount = Integer.parseInt(rs.getString(4));
	            		System.out.println("user id: " + rs.getLong(1) +
		                  " userLogin: " + rs.getString(2) +
		                  " userPassword: " + rs.getString(3) +
		                  " userAmount: " + rs.getString(4));
		            	rez = "user id: " + rs.getLong(1) + " login: " + rs.getString(2) + " password: " + rs.getString(3) + " amount: " + rs.getString(4);
	            	}
	            }
	        } catch (Exception e){
	    	   e.printStackTrace();
	    	}
	 }
	            
	private void sqlGetRecipientData() {
		ResultSet rs = null;
		Statement st = null; 
		  try{
	            st = myDB.conDB.createStatement();
	            rs = st.executeQuery("select * from clients");
	            while (rs.next()) {
	            	if(rs.getString(2).contentEquals(recipientName)) {
	            		recipientAmount = Integer.parseInt(rs.getString(4));
	            		System.out.println("user id: " + rs.getLong(1) +
		                  " recipientLogin: " + rs.getString(2) +
		                  " recipientPassword: " + rs.getString(3) +
		                  " recipientAmount: " + rs.getString(4));
		            	rez = "recipient id: " + rs.getLong(1) + " recipient login: " + rs.getString(2) + " recipient password: " + rs.getString(3) + " recipient amount: " + rs.getString(4);
	            	}
	            }
	        } catch (Exception e){
	    	   e.printStackTrace();
	    	}
	 }

	private void sqlSendToRecipient() {
    		Statement st = null; 
    		  try{
    			st = myDB.conDB.createStatement();
    	        String query = "UPDATE CLIENTS SET AMOUNT = " + recipientAmount + " WHERE LOGIN = '" + recipientName + "'";
   	            st.executeUpdate(query);
    		  } catch (Exception e){
    			  e.printStackTrace();
    		  }
    	}

	private static void sqlSetData() {
		Statement st = null; 
		  try{
  			st = myDB.conDB.createStatement();
            String query = "UPDATE CLIENTS SET AMOUNT = " + userAmount + " WHERE LOGIN = '" + userName + "'";
            st.executeUpdate(query);
		  } catch (Exception e){
			  e.printStackTrace();
		  }
	}

	private boolean sqlCheckUser(String checkUserName) {
		ResultSet rs = null;
		Statement st = null; 
		boolean ch = false;
		  try{
	  			st = myDB.conDB.createStatement();
	            rs = st.executeQuery("select * from clients");
	            while (rs.next()) {
	            	if(rs.getString(2).contentEquals(checkUserName)) {
	            		ch = true;
	            	}
	            }
		  } catch (Exception e){
			  e.printStackTrace();
		  }
		  finally {
			  if(!ch) {
				  System.out.println("Нет такого пользователя");
			  } else {
				  System.out.println("Пользователь был найден");
			  }
		  }
		  return ch;
	}
	*/
}