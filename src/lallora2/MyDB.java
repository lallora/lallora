package lallora2;

import java.io.IOException;
import java.sql.Connection;
import java.util.Date;
import java.sql.DriverManager;
import java.sql.SQLException;


public class MyDB {
	public static boolean chConnect = false;
	public static Connection conDB = null;
	private static String username = "root";
	private static String password = "destr887";
	private static final String DB_Driver = "com.mysql.cj.jdbc.Driver";
	private static final String DB_URL = "jdbc:mysql://localhost:3306/myjavadb"+
            "?verifyServerCertificate=false"+
            "&useSSL=false"+
            "&requireSSL=false"+
            "&useLegacyDatetimeCode=false"+
            "&amp"+
            "&serverTimezone=UTC";
	
	public static void connectDB() throws IOException{
		long startTime;  
		try{
	           startTime = new Date().getTime();
	           Class.forName(DB_Driver); // проверяем наличие JDBC драйвера для работы с БД
	            System.out.println("Driver loading success!");
	            conDB = DriverManager.getConnection(DB_URL, username, password); //соединение с БД
	            chConnect = true;
	            System.out.println("Connected: " + startTime);
		  } catch (ClassNotFoundException efn){
			  efn.printStackTrace(); // отработка ошибки Class.forName
			  System.out.println("JDBC драйвер не найден!");
		  } catch (SQLException esql){
			  esql.printStackTrace(); // отработка ошибок DriverManager.getConnection
			  System.out.println("ошибка SQL!");
		  }	
	}
	public static void disconnectDB() throws IOException{
		long startTime;  
		try{
	           startTime = new Date().getTime();
	           conDB.close();
	           chConnect = false;
	           System.out.println("Disconnected: " + startTime);
		  } catch (Exception e){
			  e.printStackTrace();
		  }	
	}
}
