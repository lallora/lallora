package lallora2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Exit")
public class Exit extends HttpServlet {
	private static String firstResponseStr = "<!DOCTYPE html><html><head><meta charset='utf-8'><title>options</title>"
			+"<style>body{background-color: #32cd32;}" 
			+"h1{background-color: RGB(249, 201, 16);}"
			+"p{background-color: #9acd32;	color: white;}</style>"
			+"<style>a:link{color: #FFFFFF;padding: 2px;}"
			+"a:hover{background: #786b59;color: #ffe;}</style></head>"
			+"<body><center>";	
	private static final long serialVersionUID = 1L;
    public Exit() {
        super();
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");		
		try {
			MyDB.disconnectDB();
			PrintWriter out = response.getWriter();
			out.print(firstResponseStr);
			out.println("<form method='POST' action='Authorization'><table><tr><td><center><p>Авторизация</p></center></td></tr>"
					+"<tr><td>Логин:</td></tr><td><input type='text' name='login'></td><tr></tr>"
					+"<tr><td>Пароль:</td></tr><tr><td><input type='text' name='password'></td></tr>"
					+"<tr><td><input type='submit' value='Вход'></td></tr></table><table><tr><p><center><a href='registration.html'>Регистрация</a></center></p>"
					+"</tr></table></form></center></body></html>");
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
}
