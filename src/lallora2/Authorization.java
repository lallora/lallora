package lallora2;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Authorization")
public class Authorization extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(Authorization.class.getName());
/*	private static Logger LOGGER = null;
    static {
        try(FileInputStream ins = new FileInputStream("c:\\out.conf")){ //полный путь до файла с конфигами
            LogManager.getLogManager().readConfiguration(ins);
            LOGGER = Logger.getLogger(Authorization.class.getName());
        }catch (Exception ignore){
            ignore.printStackTrace();
        }
    }
 */   
    public static UserCheck ua;
	private static final long serialVersionUID = 1L;
       
    @Override
    public void init() throws ServletException {
    	super.init();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		LOGGER.info("Logger Name: " + LOGGER.getName());
        LOGGER.warning("Can cause ArrayIndexOutOfBoundsException");


		request.setCharacterEncoding("UTF-8");
		final String userName = request.getParameter("login");
		final String userPassw = request.getParameter("password");
		int userAmount = 0;
		boolean userCheck = false;
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");		
		
		MyDB.connectDB();
		if(MyDB.chConnect) {
			ua = new UserCheck(userName, userPassw);
			if(ua.checkUser()){
				userAmount = ua.getAmount();
				userCheck = true;
			}
			PrintWriter out = response.getWriter();
			if (userCheck){
				out.print(MyHTML.firstResponseStr);
				out.print("Имя пользователя: <h3>" + userName + "</h3>");
				out.print("Сумма на счете: <h3>" + userAmount + " руб.</h3><br>");
				out.println("<b>Выполнение операций</b><br/><br/>"
						+"<form method='POST' action='Options'><table><tr><td>Добавить сумму:</td><td><input type='text' name='addAmount'></td>"
						+"</tr><tr><td>Снять сумму:</td><td><input type='text' name='getAmount'></td></tr><tr><td></td>"
						+"<td><input type='submit' value='Выполнить'></td></tr></table></form>");
						out.println("<br/><br/>");
						out.println("<b>Перевод другому пользователю</b><br/><br/>"
						+"<form method='POST' action='Transfer'><table><tr>"
						+"<td>Логин другого пользователя:</td><td><input type='text' name='recipient'></td></tr>"
						+"<tr><td>Перевести сумму:</td><td><input type='text' name='sendAmount'></td></tr><tr><td></td>"
						+"<td><input type='submit' value='Выполнить перевод'></td></tr></table></form><br/><br/>");
						out.print(MyHTML.buttonExit);
						out.close();
			}else {
				out.print(MyHTML.firstResponseStr);
				out.print("<h3>Пользователь с такими логином и паролем не найден.</h3>");
				out.print("Попробуйте еще раз или зарегистрируйтесь.");
				out.print(MyHTML.lastResponseStr);
				out.close();
				MyDB.disconnectDB();
			}
			out.close();
		} else {
			System.out.println("Не удалось установить соединение с базой данных");
		}
	}
}
	