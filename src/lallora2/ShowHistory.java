package lallora2;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ShowHistory
 */
@WebServlet("/ShowHistory")
public class ShowHistory extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResultSet rs = null;
		Statement st = null;
		int rowNumber = 5; // количество выводимых записей. можно управлять по разному, для примера сделал так
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");		
		String userName = Authorization.ua.getLogin();
		//final String userName = request.getParameter("login");
		
		MyDB.connectDB();
    	System.out.println("История. Соединение с бд завершено");
		if(MyDB.chConnect) {
			try{
				st = MyDB.conDB.createStatement();
		        rs = st.executeQuery("SELECT * FROM history WHERE username = '" + userName + "' ORDER BY ID DESC");
		    	System.out.println("История. Запрос выполнен");
		        PrintWriter out = response.getWriter();
				out.print(MyHTML.firstResponseStr);
				out.print("<h3>Страница для вывода истории операций клиента: " + userName + "</h3>");
				out.print("");
		        while (rs.next()) {
		        	if(rowNumber-->0) {
					out.println("<p>" + rs.getString(4)+ " " + rs.getString(3) + "</p>");
		        	System.out.println(rs.getString(4)+ " " + rs.getString(3));
		        	}
		        }
				out.print(MyHTML.lastResponseStr);
				out.close();
				MyDB.disconnectDB();
			} catch (Exception e){
				  e.printStackTrace(); 
				  System.out.println("Ошибка выполнения вывода истории операций клиента");
			}
		} else {
			System.out.println("Не удалось установить соединение с базой данных");
	}
}

}
