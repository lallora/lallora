package lallora2;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AddNewClient
 */
@WebServlet("/AddNewClient")
public class AddNewClient extends HttpServlet {
	public static UserCheck nua;
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddNewClient() {
        super();
        // TODO Auto-generated constructor stub
    }

    @Override
    public void init() throws ServletException {
    	super.init();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		final String newUserName = request.getParameter("newlogin");
		final String newUserPassw = request.getParameter("newpassword");
		boolean userCheck = false;
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");		
		
		MyDB.connectDB();
		if(MyDB.chConnect) {
			nua = new UserCheck(newUserName, newUserPassw);
			if(nua.checkUser()){
				userCheck = true;
			}
			PrintWriter out = response.getWriter();
			if (userCheck){
				out.print(MyHTML.firstResponseStr);
				out.print("<h3>Пользователь с такими логином уже существует. Регистрация невозможна.</h3>");
				out.print(MyHTML.lastResponseStr);
				out.close();
				MyDB.disconnectDB();
			}else {
				if(nua.regUser(newUserName, newUserPassw)) {					
					out.print(MyHTML.firstResponseStr);
					out.print("<h3>Пользователь " + newUserName + " успешно зарегистрирован.</h3>");
					out.print(MyHTML.buttonExitWithoutHistory);
					out.close();
				}
				MyDB.disconnectDB();
			}
			out.close();
		} else {
			System.out.println("Не удалось установить соединение с базой данных");
		}
	}
}
